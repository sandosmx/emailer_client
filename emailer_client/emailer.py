import requests

try:
    from django.conf import settings
except ImportError:
    settings = None


class Emailer(object):
    """Emailer Client class.
    The constructor expects a api_url and api_token for the emailer API.
    OR that you set EMAILER_API_URL and EMAILER_API_TOKEN on your settings file
    if you are using Django.
    """
    BASE_URL = 'api/emailer/'

    def __init__(self, api_url=None, api_token=None, base_url=None):
        if not base_url:
            base_url = getattr(settings, 'EMAILER_API_BASE_URL', self.BASE_URL)
        self.base_url = base_url

        if not api_url:
            api_url = getattr(settings, 'EMAILER_API_URL', '')
        self.api_url = api_url + self.base_url

        if not api_token:
            api_token = getattr(settings, 'EMAILER_API_TOKEN', '')
        self.api_token = api_token

    def _api_post(self, url, data):
        response = requests.post(
            self.api_url + url,
            json=data,  # sending data as json
            verify=False,  # avoid SSL certificate validations
            headers={'Authorization': 'Token {}'.format(self.api_token)}
        )
        return (response.json(), response.status_code)

    def _api_get(self, url, data={}):
        response = requests.get(
            self.api_url + url,
            json=data,  # sending data as json
            verify=False,  # avoid SSL certificate validations
            headers={'Authorization': 'Token {}'.format(self.api_token)}
        )
        return (response.json(), response.status_code)

    def validate(self, email, domain=None):
        """Validate a given email
        """
        url = 'v2/validate/'
        data = {'email': email}
        if domain:
            data['domain'] = domain
        return self._api_post(url, data)

    def soft_validate(self, email, domain=None):
        """Validate a given email, but setting sending_status=False for new emails
        """
        url = 'v2/soft-validate/'
        data = {'email': email}
        if domain:
            data['domain'] = domain
        return self._api_post(url, data)

    def email_status(self, email, domain=None):
        """Validate a given email
        """
        url = 'v2/email-status/'
        data = {'email': email}
        if domain:
            data['domain'] = domain
        return self._api_post(url, data)

    def subscribe(self, email, domain, details):
        """Mark email as valid and add details
        """
        url = 'v2/subscribe/'
        data = {'email': email, 'domain': domain, 'details': details}
        return self._api_post(url, data)

    def unsubscribe(self, email, domain, details):
        """Mark email as valid and add details
        """
        url = 'v2/unsubscribe/'
        data = {'email': email, 'domain': domain, 'details': details}
        return self._api_post(url, data)

    def mark_as_valid(self, email, details):
        """Mark email as valid and add details
        """
        url = 'v2/mark-as-valid/'
        data = {'email': email, 'details': details}
        return self._api_post(url, data)

    def mark_as_soft_valid(self, email, details):
        """Mark email as valid but do not modify the sending status
        """
        url = 'v2/mark-as-soft-valid/'
        data = {'email': email, 'details': details}
        return self._api_post(url, data)

    def mark_as_bounce(self, email, details):
        """Mark email as valid and add details
        """
        url = 'v2/mark-as-bounce/'
        data = {'email': email, 'details': details}
        return self._api_post(url, data)

    def simple_bulk_validation(self, domain, emails=[], callback_url=None, internal_only=False, validation_type='standard'):
        url = 'v2/simple-bulk/'
        data = {'emails': emails, 'domain': domain, 'internal_only': internal_only, 'type': validation_type}
        if callback_url:
            data.update({'callback': callback_url})
        return self._api_post(url, data)

    def simple_bulk_stats(self, bulk_id):
        url = 'v2/simple-bulk/{}/stats/'.format(bulk_id)
        return self._api_get(url)
