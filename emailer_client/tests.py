from unittest import TestCase

from mock import MagicMock, patch

from .emailer import Emailer


class EmailerTestCase(TestCase):

    def setUp(self):
        self.emailer = Emailer(api_url='example.com', api_token='fake')

    @patch('emailer_client.emailer.Emailer._api_post',
           MagicMock(return_value=[{'is_valid': True}]))
    def test_validate(self):
        # GIVEN an email
        email = 'info@example.com'
        domain = 'sandos.com'

        # WHEN we call the validate method
        response = self.emailer.validate(email, domain=domain)

        # THEN
        self.assertTrue(response[0]['is_valid'])

    @patch('emailer_client.emailer.Emailer._api_post',
           MagicMock(return_value=[{'is_valid': True, 'sending_status': False}]))
    def test_soft_validate(self):
        # GIVEN an email
        email = 'info@example.com'
        domain = 'sandos.com'

        # WHEN we call the soft_validate method
        response = self.emailer.soft_validate(email, domain=domain)

        # THEN
        self.assertTrue(response[0]['is_valid'])
        self.assertFalse(response[0]['sending_status'])

    @patch('emailer_client.emailer.Emailer._api_post',
           MagicMock(return_value=[{'is_valid': True}]))
    def test_email_status(self):
        # GIVEN an email
        email = 'info@example.com'
        domain = 'sandos.com'

        # WHEN we call the validate method
        response = self.emailer.email_status(email, domain=domain)

        # THEN
        self.assertTrue(response[0]['is_valid'])

    @patch('emailer_client.emailer.Emailer._api_post',
           MagicMock(return_value=[{'details': 'User request'}]))
    def test_mark_as_valid(self):
        # GIVEN an email and details
        email = 'info@example.com'
        details = 'User request'

        # WHEN we call the mark_as_valid method
        response = self.emailer.mark_as_valid(email, details)

        # THEN
        self.assertEqual(response[0]['details'], details)

    @patch('emailer_client.emailer.Emailer._api_post',
           MagicMock(return_value=[{'details': 'User request'}]))
    def test_mark_as_soft_valid(self):
        # GIVEN an email and details
        email = 'info@example.com'
        details = 'User request'

        # WHEN we call the mark_as_soft_valid method
        response = self.emailer.mark_as_soft_valid(email, details)

        # THEN
        self.assertEqual(response[0]['details'], details)

    @patch('emailer_client.emailer.Emailer._api_post',
           MagicMock(return_value={'sending_status': True}))
    def test_subscribe(self):
        # GIVEN an email and details
        email = 'info@example.com'
        domain = 'sandos.com'
        details = 'manual subscribe'

        # WHEN we call the unsubscribe method
        response = self.emailer.subscribe(email, domain, details)

        # THEN
        self.assertTrue(response['sending_status'])

    @patch('emailer_client.emailer.Emailer._api_post',
           MagicMock(return_value={'sending_status': False}))
    def test_unsubscribe(self):
        # GIVEN an email and details
        email = 'info@example.com'
        domain = 'sandos.com'
        details = 'list unsubscribe'

        # WHEN we call the unsubscribe method
        response = self.emailer.unsubscribe(email, domain, details)

        # THEN
        self.assertFalse(response['sending_status'])

    @patch('emailer_client.emailer.Emailer._api_post',
           MagicMock(return_value={'bounce': True}))
    def test_mark_as_bounce(self):
        # GIVEN an email and details
        email = 'info@example.com'
        details = 'Mark as bounce manual'

        # WHEN we call the mark_as_bounce method
        response = self.emailer.mark_as_bounce(email, details)

        # THEN
        self.assertTrue(response['bounce'])

    @patch('emailer_client.emailer.Emailer._api_post',
           MagicMock(return_value={'task_id': 'fake-id'}))
    def test_simple_bulk_validation(self):
        # GIVEN an email and details
        email = 'info@example.com'
        callback = 'https://emailer.sandos.com/'
        domain = 'sandos.com'

        # WHEN we call the simple_bulk_validation method
        response = self.emailer.simple_bulk_validation(domain, [email], callback_url=callback)

        # THEN
        self.assertEqual(response['task_id'], 'fake-id')

    @patch('emailer_client.emailer.Emailer._api_get',
           MagicMock(return_value={"email_count": 3}))
    def test_simple_bulk_stats(self):
        # GIVEN an bulk id
        bulk_id = 1

        # WHEN we call the simple_bulk_validation method
        response = self.emailer.simple_bulk_stats(bulk_id)

        # THEN
        self.assertEqual(response['email_count'], 3)
