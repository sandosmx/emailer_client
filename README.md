# Emailer Client

This is a client to simplify and expose all functionality of Emailer Service.

Example usage:

```python
    from emailer_client import Emailer

    results = Emailer().validate('info@example.com')
    print(results)
```

The constructor expects a api_url and api_token for the emailer API. OR that you set EMAILER_API_URL and EMAILER_API_TOKEN on your settings file if you are using Django.

exmple `settings.py` file:
```python
    EMAILER_API_URL = 'https://emailer.sandosmx.com/'
    EMAILER_API_TOKEN = 'DRF-user-token'
```

## Installation:

```
pip install git+ssh://git@bitbucket.org/sandosmx/emailer_client.git
```

## Running tests

```
python setup.py test
```