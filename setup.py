from setuptools import setup

setup(name='emailer client',
      version='0.5.0',
      description='Sandos Emailer client to operate the emailer cache',
      url='https://bitbucket.org/sandosmx/emailer_client',
      author='Rafael Capdevielle',
      author_email='capdevielle@gmail.com',
      license='MIT',
      packages=['emailer_client'],
      install_requires=[
          'requests',
      ],
      zip_safe=False,
      test_suite='nose.collector',
      tests_require=['nose', 'mock'])
